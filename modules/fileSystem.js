const fs = require('fs')
const { v4: UUID } = require('uuid')

function tryCreatePublicFolderIfDoesntExist(){
    const dir = './public'

    if(!fs.existsSync(dir)){
        fs.mkdirSync(dir)
    }
}

function generateImageName(){
    const name = UUID()
    const ext = "jpeg"
    const filename = `${name}.${ext}`
    return filename
}

/**
 * @param {String} filename 
 * @param {String} base64string 
 */
function saveBase64Image(filename, base64string){
    const base64Data = base64string.replace(/^data:image\/jpeg;base64,/, "");
    fs.writeFileSync(filename, base64Data, 'base64', function(err){
        console.log('[ERR SAVE BASE64IMG]', err)
    })
}

function deleteFile(filename){
    try{
        fs.unlinkSync(filename)
    }catch{
        // do nothing
    }
}

module.exports = {
    tryCreatePublicFolderIfDoesntExist,
    generateImageName,
    saveBase64Image,
    deleteFile
}
