require('dotenv').config()
require('./connection').connectToDatabase()

const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')

const app = express()

// Middlewares
app.use(bodyParser.json({
    limit: "100mb"
}));

// Routes
const virtualTour = require('./routes/virtualTour')
app.use('/public', express.static(path.join(__dirname, 'public')))
app.use('/api/virtual-tour', virtualTour)


const PORT = process.env.PORT || 3000
const HOST = process.env.HOST || 'localhost'

app.listen(PORT, HOST, ()=>{
    console.log(`Server started at ${HOST}:${PORT}`)
})