// @ts-check
const express = require('express')
const { isValidObjectId } = require('mongoose')
const { VirtualTour } = require('../model')
const { tryCreatePublicFolderIfDoesntExist, generateImageName, saveBase64Image, deleteFile } = require('../modules/fileSystem')
const router = express()

router.get('/', async(req, res)=>{
    const virtualTours = await VirtualTour.find({}, {
        '__v': 0,
        'rooms._id': 0,
        'rooms.points._id': 0
    })
    virtualTours.forEach(tour => tour.rooms.forEach(room => {
        room.image = `http://${process.env.HOST}:${process.env.PORT}/${room.image}`
    }))
    res.status(200).json( virtualTours )
})

router.get('/:id', async(req, res)=>{
    const { id } = req.params
    if(!isValidObjectId(id)){
        return res.status(400).json({
            msg: 'Invalid Virtual Tour ID'
        })
    }
    const virtualTour = await VirtualTour.findById(id, {
        '__v': 0,
        'rooms._id': 0,
        'rooms.points._id': 0
    })
    if(virtualTour == null){
        return res.status(404).json({
            msg: 'Virtual Tour not found!'
        })
    }
    // @ts-ignore
    virtualTour.rooms.forEach(room => {
        room.image = `http://${process.env.HOST}:${process.env.PORT}/${room.image}`
    })

    res.status(200).json( virtualTour )
})

router.post('/', async(req, res)=>{
    try{
        /**
         * @type {{ name: String, address: String, price: Number, description: String, houseArea: Number, fieldArea: Number, certificate: String?, numberOfBed: Number, numberOfBath: Number, rooms: { id: String, name: String, image: String, points: { roomId: String, x: Number, y: Number,  z: Number}[] }[] }}
         **/
        const virtualTourInput = req.body
        const rooms = virtualTourInput.rooms
        tryCreatePublicFolderIfDoesntExist()

        const isValidRoomLength = rooms.length > 0
        if(!isValidRoomLength){
            return res.status(400).json({
                err: "Virtual tour must contain at least 1 room!"
            })
        }

        for(let i=0;i<rooms.length;i++){
            const room = rooms[i]
            const { image: base64image } = room
            
            const filename = generateImageName()
            const filepath = `public/${filename}`
            saveBase64Image(filepath, base64image)

            room.image = filepath
        }
        const virtualTour = new VirtualTour(virtualTourInput)
        await virtualTour.save()

        return res.status(200).json({
            msg: 'Virtual Tour successfully created!'
        })
    }catch(err){
        return res.status(400).json({
            err: 'There\'s empty field!'
        })
    }
})

router.put('/:id', async(req, res)=>{
    const virtualTourId = req.params.id
    if(!isValidObjectId(virtualTourId)){
        return res.status(404).json({
            err: "Invalid Virtual tour ID!"
        })
    }

    const oldVirtualTour = await VirtualTour.findOne({ _id: virtualTourId })
    if(oldVirtualTour == null){
        return res.status(404).json({
            err: "Virtual tour not found!"
        })
    }
    try{
        /**
         * @type {{ name: String, address: String, price: Number, description: String, houseArea: Number, fieldArea: Number, certificate: String, numberOfBed: Number, numberOfBath: Number, rooms: { id: String, name: String, image: String, points: { roomId: String, x: Number, y: Number,  z: Number}[] }[] }}
         **/
        const virtualTourInput = req.body
        const rooms = virtualTourInput.rooms
        tryCreatePublicFolderIfDoesntExist()

        const isValidRoomLength = rooms.length > 0
        if(!isValidRoomLength){
            return res.status(400).json({
                err: "Virtual tour must contain at least 1 room!"
            })
        }

        for(let i=0;i<rooms.length;i++){
            const room = rooms[i]
            const { image: base64image } = room
            
            const filename = generateImageName()
            const filepath = `public/${filename}`
            saveBase64Image(filepath, base64image)

            room.image = filepath
        }
        const virtualTour = new VirtualTour(virtualTourInput)
        await virtualTour.save()

        await deleteVirtualTour(oldVirtualTour)

        return res.status(200).json({
            msg: 'Virtual Tour successfully updated!'
        })
    }catch(err){
        return res.status(400).json({err})
    }
})

router.delete('/:id', async(req, res)=>{
    const virtualTourId = req.params.id
    if(!isValidObjectId(virtualTourId)){
        return res.status(404).json({
            err: "Invalid Virtual tour ID!"
        })
    }
    const virtualTour = await VirtualTour.findById(virtualTourId)
    if(virtualTour == null){
        return res.status(404).json({
            err: "Virtual tour not found!"
        })
    }

    await deleteVirtualTour(virtualTour)

    return res.status(200).json({
        msg: 'Virtual Tour successfully deleted!'
    })
})

async function deleteVirtualTour(virtualTour){
    virtualTour.rooms.forEach(room => deleteFile(room.image))
    await VirtualTour.findByIdAndDelete(virtualTour._id)
}

module.exports = router
