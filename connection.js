const mongoose = require('mongoose')
require('dotenv').config()

async function connectToDatabase(){
    return await mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
}

module.exports = { 
    connectToDatabase
}
