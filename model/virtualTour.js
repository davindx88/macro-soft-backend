const mongoose = require('mongoose');

const virtualTourSchema = new mongoose.Schema({
    name: { type: String, required: true },
    address: { type: String, required: true, default: "" },
    description: { type: String, required: true },
    fieldArea: { type: Number, required: true, default: 0 },
    houseArea: { type: Number, required: true },
    certificate: { type: String, required: false, default: "" },
    numberOfBed: { type: Number, required: true },
    numberOfBath: { type: Number, required: true },
    price: { type: Number, required: true },
    rooms: {
        type: [{
            id: { type: String, required: true },
            name: { type: String, required: true },
            image: { type: String, required: true },
            points: {
                type: [{ 
                    roomId: { type: String, required: true }, 
                    x: { type: Number, required: true }, 
                    y: { type: Number, required: true }, 
                    z: { type: Number, required: true }, 
                }],
                required: true
            }
        }],
        required: true
    }
});

const VirtualTour = mongoose.model('VirtualTour', virtualTourSchema);

module.exports = VirtualTour
